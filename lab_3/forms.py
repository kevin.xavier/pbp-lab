from django import forms
from django.forms import ModelForm, widgets
from lab_1.models import Friend


class DateInput(forms.DateInput):
    input_type = 'date'
    
class FriendForm(ModelForm):
   class Meta:
       model = Friend
       fields = "__all__"
       widgets = {
           'birth_date': DateInput()
       }
    # ['name', 'npm', 'birth_date']
