import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.cyan[200],
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: const Center(
            child: Text(
              'Covid Cares Homepage',
              style: TextStyle(
                color: Colors.black87,
                fontSize: 24,
              ),
            ),
          ),
        ),
        body: Center(
          child: Column(children: [
            Card(
              child: InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: () {},
                child: const SizedBox(
                  width: 300,
                  height: 100,
                  child: Center(
                    child: Text(
                      'Vaccine',
                      style: TextStyle(
                        color: Colors.black87,
                        fontSize: 24,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: () {},
                child: const SizedBox(
                  width: 300,
                  height: 100,
                  child: Center(
                    child: Text(
                      'Medicine',
                      style: TextStyle(
                        color: Colors.black87,
                        fontSize: 24,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Card(
              child: InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: () {},
                child: const SizedBox(
                  width: 300,
                  height: 100,
                  child: Center(
                    child: Text(
                      'COVID - 19 Info',
                      style: TextStyle(
                        color: Colors.black87,
                        fontSize: 24,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
