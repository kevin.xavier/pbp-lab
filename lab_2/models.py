from django.db import models

class Note(models.Model):
    TO = models.CharField(max_length=30)
    FROM = models.CharField(max_length=30)
    TITLE = models.CharField(max_length=30)
    MESSAGE = models.CharField(max_length=100)
