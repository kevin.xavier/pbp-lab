from django.db import models

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here DONE


class Friend(models.Model):
    name = models.CharField(max_length=30)
    # TODO Implement missing attributes in Friend model DONE
    npm = models.CharField(max_length=10)
    birth_date = models.DateField()
