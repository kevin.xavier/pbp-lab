Apakah perbedaan antara JSON dan XML?
JSON dan XML sama - sama dapat digunakan untuk menyimpan dan
mengirimkan data. Perbedaan di antara keduanya terdapat pada
bagaimana cara mereka menyimpan data. JSON menyimpan data
dengan konsep key and value yang mirip dengan dictionary python.
Di sisi lain, XML menyimpan data dalam bentuk tree
yang di dalamnya terdapat elemen - elemen dari data tersebut.

Apakah perbedaan antara HTML dan XML?
Perbedaan antara HTML dan XML terdapat pada fungsionalitasnya.
Meskipun keduanya dapat digunakan untuk membuat web page ataupun
web application, HTML bersifat statis dan lebih digunakan untuk
menampilkan data kepada klien. Di sisi lain, XML bersifat dinamis
dan lebih sering digunakan untuk penyimpanan dan transportasi data. 
